A simple [blag](https://xkcd.com/148/) engine for GAE. Mainly there to freshen my GAE skills. 

Test version is here: https://education-impunity-hatch.appspot.com/ . 

You can create user account and then add blogs, posts, etc. 

# Instructions

## Local developement 

1. Create virtualenv using python2.7
2. pip install -r `requirements_dev.txt` 
2. pip install -r `requirements.txt`  
3. pip install -r `requirements.txt` -t lib
4. pip install -r `requirements_dev.txt` -t lib
5. cp `settings_example.yml` `settings.yml` (Or just fill settings by yourself!)

## Deployment  

1. `appcfg.py (...) update .` 
  
# Quirks

1. ``Blag`` is not a [spelling mistake](https://xkcd.com/148/).   
2. Code is intended by two spaces. 
3. JS files are generated from `.coffeescript` files. Generation is done by my IDE. 
   When I'll need to create a makefile for this project, it will not still be a hobby project. 
   
# "Things to note"  

1. I wrote login code --- assumed this was part of the excercise, normally I would use google 
   login. 
2. Probably I should do some e-mail verification.   
3. I used singed session cookies to store login information, I dislike that and wouldn't use this 
   on production --- normally I would use some libary. 
3. Analytics are very simplified --- only blog archive is generated. 
