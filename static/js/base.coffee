

$ () ->

  $link = $('#logout-link');

  $link.click (e) =>
    $("<form>").attr("method", "POST").attr("action", $link.attr("href"))
    .appendTo(document.body).submit()
    e.preventDefault()
    return false

$ () ->
  $('[data-toggle="tooltip"]').tooltip()