import functools

import flask
from flask.helpers import url_for
from . import models, hash_utils


def login(email=None, user=None):
  if email is None and user is None:
    raise TypeError("Please provide at least email or login")

  if user is None:
    user = models.User.get_by_id()

  from flask import session, g
  session['logged_in_user'] = user.email
  g.current_user = user


def logout():
  from flask import session, g
  g.current_user = None
  del session['logged_in_user']


def verify_passsword_and_login(email, password):
  try:
    user = models.User.get_by_email(email)
  except models.UserDoesNotExist:
    return False
  password_ok = hash_utils.verify_password(user.password_hash, password)
  if password_ok:
    login(user=user)
  return password_ok


def attach_user():
  from flask import session, g
  g.current_user = None
  try:
    user = session['logged_in_user']
    g.current_user = models.User.get_by_email(user)
  except (models.UserDoesNotExist, KeyError):
    pass


def create_user(email, password, username):
  user = models.User(email=email)
  user.username = username
  user.set_password(password)
  user.put()
  return user


def require_login(view):

  @functools.wraps(view)
  def require_login_wrapper(*args, **kwargs):
    if not is_logged_in():
      path = flask.request.path
      if not 'login' in path:
        flask.session['post_login_path'] = path
      return flask.redirect(url_for("auth.login"))
    return view(*args, **kwargs)

  return require_login_wrapper


def is_logged_in():
  return flask.g.current_user is not None


def current_user():
  from flask import g
  user = g.current_user
  if user is None:
    raise ValueError()
  return user