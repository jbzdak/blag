from flask_wtf import Form

import wtforms
from wtforms import validators

from .models import User


class ValidateEmailUnique(object):

  def __call__(self, form, field, message=None):
    other_users = User.query().filter(User.email == field.data).get()
    # I know that this is a race condition, but there appears to be no
    # unique constraint in GAE + I don't want all users to have
    # the same ancestor.
    if other_users:
      raise validators.StopValidation("Email {} already taken".format(field.data))


class ValidateUsernameUnique(object):

  def __call__(self, form, field, message=None):
    other_users = User.query().filter(User.username == field.data).get()
    # I know that this is a race condition, but there appears to be no
    # unique constraint in GAE + I don't want all users to have
    # the same ancestor.
    if other_users:
      raise validators.StopValidation("Username {} already taken".format(field.data))


class LoginForm(Form):

  email = wtforms.StringField(
    label="Your email",
    description="Please double check, email will be sent for verification purposes",
    validators=[validators.DataRequired(), validators.Email()]
  )
  password = wtforms.PasswordField(
    label="Password",
    validators=[validators.DataRequired()]
  )
  submit = wtforms.SubmitField()


class RegisterForm(Form):

  username = wtforms.StringField(
    label="Your username",
    description="Will be used to sign your post",
    validators=[validators.DataRequired(), ValidateUsernameUnique()]
  )

  email = wtforms.StringField(
    label="Your email",
    description="Please double check, email will be sent for verification purposes",
    validators=[validators.DataRequired(), validators.Email(), ValidateEmailUnique()]
  )
  password = wtforms.PasswordField(
    label="Password",
    description="Please choose complex password",
    validators=[validators.DataRequired()]
  )
  password_repeat = wtforms.PasswordField(
    label="Repeat the password",
    validators=[validators.DataRequired(), validators.EqualTo('password')]
  )
  submit = wtforms.SubmitField()
