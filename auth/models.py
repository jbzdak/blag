import base64
from google.appengine.ext import ndb

from . import hash_utils


class UserDoesNotExist(Exception): pass


class User(ndb.Model):

  username = ndb.StringProperty()
  email = ndb.StringProperty()
  password_hash_string = ndb.StringProperty()
  is_active = ndb.BooleanProperty(default=False)
  is_staff = ndb.BooleanProperty(default=False)

  @property
  def is_anonymous(self):
    return False

  @classmethod
  def get_by_email(cls, email):
    user = User.query().filter(User.email==email).get()
    if user is None:
      raise UserDoesNotExist()
    return user

  @classmethod
  def get_by_username(cls, username):
    user = User.query().filter(User.username==username).get()
    if user is None:
      raise UserDoesNotExist()
    return user

  @property
  def password_hash(self):
    return hash_utils.HashedPassword(*map(base64.b64decode, self.password_hash_string.split("$")))

  @password_hash.setter
  def password_hash(self, hash):
    self.password_hash_string = "$".join(map(base64.b64encode, hash))

  def set_password(self, password):
    self.password_hash = hash_utils.create_hashed_password(password)

  def verify_password(self, password):
    return hash_utils.verify_password(self.password_hash, password)


class AnonymousUser(object):
  email = None
  is_active = False
  is_staff = False
  is_anonymous = True


class VerificationCode(ndb.Model):
  expiry = ndb.DateTimeProperty()
  code = ndb.StringProperty()
