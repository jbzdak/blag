
def Auth(app):
  from .login_utils import attach_user
  from .blueprint import auth, inject_user
  app.before_request(attach_user)
  app.register_blueprint(auth)
  app.context_processor(inject_user)