import collections
import hashlib
import hmac
import os

HashedPassword = collections.namedtuple(
  "HashedPassword", ['algorithm', 'salt', 'hash']
)
"""
Salt and data are byte strings.
"""


def _hash_password(password, salt, method):
  from flask import current_app
  pepper = current_app.config['PASSWORD_PEPPER']
  digestmod = getattr(hashlib, method)
  digest = hmac.HMAC("{}:{}".format(pepper, salt), digestmod=digestmod)
  digest.update(password)
  return digest.digest()


def create_hashed_password(password):
  from flask import current_app
  salt = os.urandom(current_app.config['PASSWORD_SALT_LENGTH_BYTES'])
  algorithm = current_app.config['PASSWORD_PREFFERED_HASH']
  hashed_password = _hash_password(password, salt, algorithm)
  return HashedPassword(algorithm, salt, hashed_password)


def compare_digest(string1, string2):
  # TODO: Remove this once hmac.compare_digest works on GAE
  return 0 == sum(i != j for i, j in zip(string1, string2))


def verify_password(hashed_password, password):
  current_hash = _hash_password(password, hashed_password.salt, hashed_password.algorithm)
  return compare_digest(hashed_password.hash, current_hash)

