import base64

import os
import pytest

from . import models
from . import hash_utils


PEPPER = os.urandom(16)


test_data = [
  # Let's start with most common password ;)
  "123456",
  "12345678",
  "baseball",
  "correct-horse-battery-staple",
  base64.b64encode(os.urandom(256))
]


@pytest.yield_fixture(scope="module", autouse=True)
def set_up():
  from webapp.main import app
  with app.app_context():
    yield


@pytest.mark.parametrize("password", test_data)
def test_hash_create(password):
  from flask import current_app
  result = hash_utils.create_hashed_password(password)
  assert len(result.salt) == current_app.config['PASSWORD_SALT_LENGTH_BYTES']
  assert result.algorithm == current_app.config['PASSWORD_PREFFERED_HASH']


@pytest.mark.parametrize("password", test_data)
def test_hash_verify(password):
  hashed_pass = hash_utils.create_hashed_password(password)
  hash_utils.verify_password(hashed_pass, password)


def test_model_setter():
  user = models.User()
  hashed_password = hash_utils.HashedPassword(
    "algorithm", "salt", "hash"
  )
  user.password_hash = hashed_password
  assert user.password_hash == hashed_password
