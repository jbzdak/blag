
import flask
from flask import Blueprint, render_template, g, redirect, current_app

from . import login_utils, models, forms

auth = Blueprint('auth', __name__)


@auth.context_processor
def inject_user():
  current_user = g.current_user
  if current_user is None:
    return {
      "user": models.AnonymousUser()
    }
  return {
    "user": g.current_user
  }


@auth.route('/login', methods=('POST', 'GET'))
def login():
  form = forms.LoginForm()
  if form.validate_on_submit():
    login_ok = login_utils.verify_passsword_and_login(
        email=form.data['email'],
        password=form.data['password'])
    if login_ok:
      if 'post_login_path' in flask.session:
        redirect_path = flask.session['post_login_path']
        del flask.session['post_login_path']
        return redirect(redirect_path)
      redirect_to = current_app.config.get('AUTH_LOGIN_REDIRECT')
      if redirect_to is None:
        return redirect("/")
      return redirect(flask.url_for(redirect_to))
    form.password.errors.append(
      "Invalid password or username"
    )
  return render_template(
    'form.html',
    form = form,
    page_title = "Login"
  )


@auth.route('/logout', methods=('POST', ))
def logout():
  login_utils.logout()
  redirect_to = current_app.config.get('AUTH_LOGOUT_REDIRECT')
  if redirect_to is None:
    return redirect("/")
  return redirect(flask.url_for(redirect_to))


@auth.route('/register', methods=('POST', 'GET'))
def register():
  form = forms.RegisterForm()
  if form.validate_on_submit():
    user = login_utils.create_user(
      email=form.data['email'],
      password=form.data['password'],
      username = form.data['username']
    )
    login_utils.login(user=user)
  return render_template(
    'form.html',
    form = form,
    page_title = "Register"
  )
