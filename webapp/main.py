#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

from flask import Flask
from flask.ext.bootstrap import Bootstrap
from auth import Auth
from blag import Blag
from flask.templating import render_template

import yaml

ROOT_DIR = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]

template_dir = os.path.join(
  ROOT_DIR,
  "templates"
)

app = Flask(__name__, template_folder=template_dir)
Bootstrap(app)
Auth(app)
Blag(app)

with open(os.path.join(ROOT_DIR, "settings_shared.yml")) as f:
  app.config.update(yaml.load(f))

settings_file = os.path.join(ROOT_DIR, "settings.yml")

if os.path.exists(settings_file):
  with open(settings_file) as f:
    app.config.update(yaml.load(f))

@app.context_processor
def static_url():
  return {
    "STATIC_URL": app.config['STATIC_URL']
  }

@app.route('/')
def main_page():
  from blag.models import Blag
  return render_template(
    'index.html',
    blags = Blag().query().order(-Blag.last_update).fetch(limit=15)
  )

