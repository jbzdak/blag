import datetime

from flask_wtf import Form

import wtforms
from wtforms import validators

from . import models


class AddBlagForm(Form):

  title = wtforms.StringField(
    label="Blog title",
    validators=[validators.DataRequired()]
  )

  text_md = wtforms.TextAreaField(
    label="Blog desctiption",
    description="Please provide markdown formatted text"
  )

  submit = wtforms.SubmitField(
    label="Create Blog!"
  )

  def save_blag(self, owner):
    blag = models.Blag()
    blag.title = self.data['title']
    blag.text_md = self.data['text_md']
    blag.owners.append(owner.key)
    blag.put()
    return blag


class AddBlagPost(Form):

  title = wtforms.StringField(
    label="Post title",
    validators=[validators.DataRequired()]
  )

  text_md = wtforms.TextAreaField(
    label="Contents",
    description="Please provide markdown formatted text"
  )

  tags_string = wtforms.StringField(
    label="Post tags",
    description="Please provide a comma separated list"
  )

  submit = wtforms.SubmitField(
    label="Create Post!"
  )

  def save_post(self, blag, author):
    post = models.BlagPost()
    post.blag = blag.key
    if author.key not in post.authors:
     post.authors.append(author.key)
    post.create_date = datetime.datetime.now()
    post.edit_date = datetime.datetime.now()
    post.tags_string = self.data['tags_string']
    post.text_md = self.data['text_md']
    post.title = self.data['title']
    post.put()
    return post


class BlagPagination(Form):

  posts = wtforms.IntegerField(
    label="Number of posts to show",
    validators = [validators.NumberRange(min=1, max=100)],
    default=20
  )

  start_from = wtforms.IntegerField(
    label="Start with post",
    default=0,
    validators=[validators.NumberRange(min=0)]
  )

  def validate_csrf_token(self, *args, **kwargs):
    # There is an outstanding bug in https://github.com/lepture/flask-wtf/issues/173
    # which disables honoring of csrf_enabled setting.
    pass
