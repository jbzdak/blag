
def Blag(app, prefix = "/blog"):
  from .blueprint import blag, update_context_logged_in
  app.register_blueprint(
    blag, url_prefix=prefix
  )
  app.context_processor(update_context_logged_in)
