from google.appengine.api import memcache, taskqueue

from blag_gae_helpers import make_memcache_key_for_user, get_from_memcache
from . import models
from auth import login_utils


def make_memcache_key_for_blag(blag, *key_parts):
  parts = [
    "blag",
    blag.key.urlsafe(),
    blag.last_update.isoformat(),
  ]

  parts.extend(map(str, key_parts))

  return ":".join(parts)


def make_memcache_key(*key_parts):
  return ":".join(map(str, key_parts))


def get_user_blags():

  user_blags_key = make_memcache_key_for_user("user_blags")

  return get_from_memcache(
      user_blags_key, lambda: models.Blag.owner_by_blog(login_utils.current_user().key)
  )

def flush_user_blags(user_key=None):
  user_blags_key = make_memcache_key_for_user("user_blags", user_key)
  memcache.delete(user_blags_key)


def update_blag_stats(blag_key):
  taskqueue.add(
      url='/workers/stats/blag/{}'.format(blag_key.urlsafe()),
      target='worker'
  )

