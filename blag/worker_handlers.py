
import collections

import datetime
from operator import itemgetter

import webapp2

from google.appengine.ext import ndb

from . import models


class CalculateBlag(webapp2.RequestHandler):
  def post(self, blag_key):
    blag_key = ndb.Key(urlsafe=blag_key)
    calculate_blag_stats(blag_key)


def calculate_blag_stats(blag_key):
  """
  I know I could do it with map-reduce --- please trust me on that :)
  """
  posts = models.BlagPost.query(models.BlagPost.blag == blag_key)
  stats = models.BlagStats(parent=blag_key)
  stats.blag = blag_key
  stats_counter = collections.Counter()
  for post in posts:
    real_date = post.create_date.date()
    month = datetime.date(
      year=real_date.year,
      month=real_date.month,
      day=1
    )
    stats_counter[month]+=1

  stats.stats = [
    models.BlagArchiveStats(
      year_month=year,
      number_of_posts=posts
    )
    for year, posts in sorted(
      stats_counter.items(), key=itemgetter(0), reverse=True
    )
  ]

  def update():
    for stat in models.BlagStats().query(ancestor=blag_key).fetch():
      stat.key.delete()
    stats.put()


  ndb.transaction(update)

app = webapp2.WSGIApplication([
  ('/workers/stats/blag/(.*)', CalculateBlag),
])