import datetime

import os

from google.appengine.ext import ndb
import flask
from blag.utils import make_memcache_key_for_blag, make_memcache_key
from blag_gae_helpers import object_or_404, make_memcache_key_for_user, get_from_memcache
from flask import Blueprint, render_template, abort, g, redirect, current_app
from flask.helpers import url_for

from auth import login_utils
from auth import models as login_models

from . import models, forms, utils


CURRENT_DIR = os.path.split(os.path.abspath(__file__))[0]

blag = Blueprint(
  'blag', __name__,
  template_folder=os.path.join(CURRENT_DIR, "templates")
)

@blag.route('/add', methods=['GET', 'POST'])
@login_utils.require_login
def add_blag():
  form = forms.AddBlagForm()
  if form.validate_on_submit():
    blag = form.save_blag(login_utils.current_user())
    return redirect(url_for("blag.add_post", blag_key=blag.key.urlsafe()))
  return render_template(
    "markdown_form.html",
    page_title="Add Blog",
    form=form,
    edited_object_key="blag_description"
  )


def __add_or_edit_post(blag_key, post=None):
  blag_key = ndb.Key(urlsafe=blag_key)
  blag = blag_key.get()
  if login_utils.current_user().key not in blag.owners:
    abort(403)
  if blag is None:
    flask.abort(404)
  form = forms.AddBlagPost(obj=post)
  if form.validate_on_submit():
    post = form.save_post(blag, login_utils.current_user())
    return redirect(url_for("blag.post_detail", post_key=post.key.urlsafe()))
  return render_template(
    "markdown_form.html",
    page_title="Add Blog Post",
    form=form,
    edited_object_key = post.key if post is not None else "new_post"
  )


@blag.route("/addpost/<blag_key>", methods=['GET', 'POST'])
@login_utils.require_login
def add_post(blag_key):
  return __add_or_edit_post(blag_key)


@blag.route("/post/edit/<post_key>", methods=['GET', 'POST'])
@login_utils.require_login
def edit_post(post_key):
  post_key = ndb.Key(urlsafe=post_key)
  post = post_key.get()
  if post is None:
    flask.abort(404)
  return __add_or_edit_post(post.blag.urlsafe(), post)


@blag.route("/view/<post_key>", methods=['GET'])
def post_detail(post_key):
  post_key = ndb.Key(urlsafe=post_key)
  post = post_key.get()
  return render_template(
    "blag_post.html",
    post=post,
    blag=post.blag.get()
  )


def do_pagination(offset, posts_to_load, query, cache_key):
  context = {}

  results, cursor, more = get_from_memcache(
    cache_key, lambda: query.fetch_page(posts_to_load, offset=offset),
    expiration_sec=600
  )

  previous_offset = offset - posts_to_load

  if previous_offset > 0:
    context['prev_url'] = "?start_from={}&posts={}".format(
      previous_offset, posts_to_load
    )

  if more:
    context['next_url'] = "?start_from={}&posts={}".format(
      offset + posts_to_load, posts_to_load
    )

  return results, context


@blag.route("/list/<blag_key>", methods=['GET'])
@blag.route("/list/<blag_key>/<year>/<month>", methods=['GET'])
def blag_view(blag_key, year=None, month=None):
  try:
    if year is not None and month is not None:
      year = int(year)
      month = int(month)
  except ValueError:
    flask.abort(400)

  blag = object_or_404(blag_key)
  posts = models.BlagPost.query(models.BlagPost.blag == blag.key).order(
    -models.BlagPost.create_date
  )
  if year is not None and month is not None:
    end_date = datetime.datetime.combine(
      datetime.date(year=year, month=month+1, day=1),
      datetime.time()
    )
    start_date = datetime.datetime.combine(
      datetime.date(year=year, month=month, day=1),
      datetime.time()
    )
    posts = posts.filter(
      models.BlagPost.create_date >= start_date,
      models.BlagPost.create_date < end_date,
    )

  form = forms.BlagPagination(formdata=flask.request.args, csrf_enabled=False)

  if not form.validate():
    flask.abort(400)

  cache_key = make_memcache_key_for_blag(
    blag, "post_list", form.data['start_from'], form.data['posts'],
    year, month
  )

  posts, context = do_pagination(
    offset=form.data['start_from'],
    posts_to_load=form.data['posts'],
    query=posts,
    cache_key=cache_key
  )

  return render_template(
    "blag_listing.html",
    blag=blag,
    posts=posts,
    blag_stats=blag.get_stats(),
    **context
  )


@blag.route("/list/tag/<tag>", methods=['GET'])
def blag_view_tag(tag):
  form = forms.BlagPagination(formdata=flask.request.args, csrf_enabled=False)
  if not form.validate():
    flask.abort(400)
  posts = models.BlagPost.query(models.BlagPost.tags == tag).order(
    -models.BlagPost.create_date
  )
  cache_key = make_memcache_key("post_by_tag", tag, form.data['start_from'], form.data['posts'])

  posts, context = do_pagination(
    offset=form.data['start_from'],
    posts_to_load=form.data['posts'],
    query=posts,
    cache_key=cache_key
  )

  return render_template(
    "post_list_by_tag.html",
    posts=posts,
    tag=tag,
    **context
  )


@blag.route("/list/author/<author>", methods=['GET'])
def blag_view_author(author):
  author_user = None
  try:
    author_user = login_models.User.get_by_username(username=author)
  except login_models.UserDoesNotExist:
    abort(404)
  form = forms.BlagPagination(formdata=flask.request.args, csrf_enabled=False)
  if not form.validate():
    flask.abort(400)
  posts = models.BlagPost.query(models.BlagPost.authors == author_user.key).order(
    -models.BlagPost.create_date
  )
  cache_key = make_memcache_key("post_by_author", author_user.key, form.data['start_from'], form.data['posts'])

  posts, context = do_pagination(
    offset=form.data['start_from'],
    posts_to_load=form.data['posts'],
    query=posts,
    cache_key=cache_key
  )

  return render_template(
    "post_list_by_author.html",
    posts=posts,
    author=author_user,
    **context
  )


def __is_blag_owner(blag):
  if not login_utils.is_logged_in():
    return False
  if not isinstance(blag, models.Blag):
    blag = blag.get()
  return login_utils.current_user().key in blag.owners


def update_context_logged_in():

  if not login_utils.is_logged_in():
    return {
      "blag_owner": __is_blag_owner,
    }
  return {
    "user_blags": utils.get_user_blags(),
    "blag_owner": __is_blag_owner,
  }