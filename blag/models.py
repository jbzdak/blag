from datetime import datetime

from google.appengine.ext import ndb

import markdown

from auth.models import User


class MarkdownTextMixin(object):
  text_md_db = ndb.TextProperty()
  text_html_db = ndb.TextProperty()

  @property
  def text_md(self):
    return self.text_md_db

  @text_md.setter
  def text_md(self, val):
    self.text_md_db = val
    self.text_html_db = markdown.markdown(val)


class Blag(MarkdownTextMixin, ndb.Model):

  title = ndb.StringProperty()
  owners = ndb.KeyProperty(kind=User, repeated=True)
  last_update = ndb.DateTimeProperty(required=True)

  @classmethod
  def owner_by_blog(cls, owner_key):
    # TODO: Don't care if someone has more blogs
    return Blag.query().filter(Blag.owners == owner_key).fetch(limit=20)

  def put(self, **ctx_options):
    for o in self.owners:
      from . import utils
      utils.flush_user_blags(o)
    self.last_update = datetime.now()
    super(Blag, self).put(**ctx_options)

  def get_stats(self):
    return BlagStats.query(ancestor=self.key).get()


class BlagPost(MarkdownTextMixin, ndb.Model):

  title = ndb.StringProperty()
  authors = ndb.KeyProperty(kind=User, repeated=True)
  create_date = ndb.DateTimeProperty(auto_now_add=True)
  edit_date = ndb.DateTimeProperty(auto_now=True)
  tags = ndb.StringProperty(repeated=True)

  blag = ndb.KeyProperty(kind=Blag)

  def put(self, **ctx_options):
    from . import utils
    blag = self.blag.get()
    blag.last_update = datetime.now()
    blag.put()
    utils.update_blag_stats(self.blag)
    super(BlagPost, self).put(**ctx_options)

  @property
  def tags_string(self):
    return ", ".join(sorted(self.tags))

  @tags_string.setter
  def tags_string(self, val):
    self.tags = [tag.strip() for tag in val.split(",")]

  @property
  def first_author(self):
    return self.authors[0].get().username


class BlagArchiveStats(ndb.Model):

  year_month = ndb.DateProperty(required=True)
  number_of_posts = ndb.IntegerProperty(required=True)


class BlagStats(ndb.Model):
  # Blag is an ancestor to this model
  stats = ndb.LocalStructuredProperty(BlagArchiveStats, repeated=True)

