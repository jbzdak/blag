from google.appengine.api import memcache

import flask
from google.appengine.ext import ndb

from auth.login_utils import current_user


def object_or_404(key):
  object_key = ndb.Key(urlsafe=key)
  object = object_key.get()
  if object is None:
    flask.abort(404)
  return object


def make_memcache_key_for_user(key_name, user_key=None):
  if user_key is None:
    user_key = current_user().key

  parts = [
    'user',
    user_key.urlsafe(),
    key_name
  ]
  return ":".join(parts)


def get_from_memcache(key, generator_function, expiration_sec=1500):
  value = memcache.get(key)
  if value is None:
    value = generator_function()
    memcache.set(key, value, time=expiration_sec)
  return value
